<!DOCTYPE HTML><html><head><meta charset="utf-8"><title>Announcing if_chain « lambda fairy</title><link href="/styles/styles.css" rel="stylesheet"><link href="/styles/hk-pyg.css" rel="stylesheet"><link href="//fonts.googleapis.com/css?family=Cabin:400,700,400italic,700italic" rel="stylesheet"><meta content="#5c3566" name="theme-color"><meta content="width=device-width" name="viewport"></head><body><header><h1><a href="/">lambda fairy</a></h1></header><div id="midriff"><nav><ul><li><a href="/">Home</a></li><li><a href="/cv">CV</a></li><li class="current"><a href="/blog">Blog</a></li><li><a href="https://github.com/lfairy">GitHub</a></li></ul></nav><section><h1>Announcing if_chain</h1><p>Today I published <a href="https://github.com/lfairy/if_chain"><code>if_chain</code></a>, a macro for writing nested <code>if let</code> expressions. The <a href="https://docs.rs/if_chain">documentation</a> does a good job of showing how to use the crate, and I recommend taking a look through it. This article will instead go more into the background behind this macro and how it helped with my own projects.</p>
<h2 id="the-problem">The problem</h2>
<p>As part of <a href="https://github.com/lfairy/maud/issues/66">another project</a>, I was working on a <a href="https://doc.rust-lang.org/book/compiler-plugins.html#lint-plugins">lint plugin</a> that catches common mistakes and suggests ways to fix them. Unfortunately, the code I wrote would often look like this:</p>
<div class="sourceCode"><pre class="sourceCode rust"><code class="sourceCode rust"><span class="kw">if</span> <span class="kw">let</span> ExprCall(<span class="kw">ref</span> path_expr, <span class="kw">ref</span> args) = expr.node <span class="op">{</span>
    <span class="kw">if</span> <span class="kw">let</span> <span class="cn">Some</span>(first_arg) = args.first() <span class="op">{</span>
        <span class="kw">if</span> <span class="kw">let</span> ExprLit(<span class="kw">ref</span> lit) = first_arg.node <span class="op">{</span>
            <span class="kw">if</span> <span class="kw">let</span> LitKind::<span class="bu">Str</span>(s, _) = lit.node <span class="op">{</span>
                <span class="kw">if</span> s.as_str().eq_ignore_ascii_case(<span class="st">&quot;&lt;!doctype html&gt;&quot;</span>) <span class="op">{</span>
                    <span class="co">// ...</span></code></pre></div>
<p>As you can see, a common issue was <em>rightward drift</em>. Every <code>if</code> statement would indent the code by one more step, such that the actual message ended up off the page!</p>
<p>Now, Rust does provide tools for tackling this issue; and in most cases it would be enough to use them. But for my use case—writing lints—they are not enough:</p>
<ul>
<li><p>We can rewrite each check to yield an <code>Option</code>, and use <code>.and_then()</code> or the <code>?</code> operator to chain them. But when writing a lint, the interfaces involved are so broad and irregular that wrapping everything is not practical.</p></li>
<li><p>We can try to merge all these checks into a single pattern. But in this case, the intermediate nodes are wrapped in smart pointers (the <a href="https://manishearth.github.io/rust-internals-docs/syntax/ptr/struct.P.html"><code>P</code></a> type), and current Rust doesn’t have a way to dereference a smart pointer from within a pattern.</p></li>
</ul>
<h2 id="existing-solutions">Existing solutions</h2>
<p>I wasn’t the first to run into this problem. <a href="https://github.com/Manishearth/rust-clippy">rust-clippy</a>, a collection of general-purpose lints, has a utility macro called <a href="https://github.com/Manishearth/rust-clippy/blob/5d78485a81c06a621f607f3e772add628c892b13/clippy_lints/src/utils/mod.rs#L36-L91"><code>if_let_chain!</code></a> for this purpose. Using this macro, the example above would be written like this instead:</p>
<div class="sourceCode"><pre class="sourceCode rust"><code class="sourceCode rust"><span class="pp">if_let_chain!</span> <span class="op">{[</span>
    <span class="kw">let</span> ExprCall(<span class="kw">ref</span> path_expr, <span class="kw">ref</span> args) = expr.node,
    <span class="kw">let</span> <span class="cn">Some</span>(first_arg) = args.first(),
    <span class="kw">let</span> ExprLit(<span class="kw">ref</span> lit) = first_arg.node,
    <span class="kw">let</span> LitKind::<span class="bu">Str</span>(s, _) = lit.node,
    s.as_str().eq_ignore_ascii_case(<span class="st">&quot;&lt;!doctype html&gt;&quot;</span>),
    <span class="co">// ...</span>
<span class="op">]</span>, <span class="op">{</span>
    <span class="co">// ...</span>
<span class="op">}}</span></code></pre></div>
<p>This solved the rightward drift problem at hand. But as I used the macro, I found a few flaws in the implementation:</p>
<ul>
<li><p>Since <code>if_let_chain!</code> is a part of Clippy, I would have to either copy-and-paste the macro, or depend on the whole of Clippy. It would be better if the macro was in its own crate.</p></li>
<li><p>When inspecting the type of an expression, for example, the code involved can be quite long. One would use intermediate variables (<code>let</code> statements) to keep the code easy to read. But since <code>if_let_chain!</code> expects every line to be an <code>if</code> or <code>if let</code>, there’s no good way of doing this refactoring.</p></li>
<li><p>Some of the syntax choices, like omitting the <code>if</code> from each check and the use of square brackets, seem arbitrary to me. I’d prefer it if the macro looks more like the generated code.</p></li>
</ul>
<h2 id="introducing-if_chain">Introducing <code>if_chain</code></h2>
<p>Here’s where <code>if_chain</code> comes in. It addresses the points raised above, and adds some features of its own:</p>
<ul>
<li><p><em>Fallback values</em>. <code>if_chain!</code> lets you give an <code>else</code> clause, which is evaluated when any of the checks fail to match.</p></li>
<li><p><em>Multiple patterns</em>. Rust allows for matching multiple patterns at once in a <code>match</code> expression. For example, this code:</p>
<div class="sourceCode"><pre class="sourceCode rust"><code class="sourceCode rust"><span class="kw">let</span> x = <span class="dv">1</span>;
<span class="kw">match</span> x <span class="op">{</span>
    <span class="dv">1</span> | <span class="dv">2</span> =&gt; <span class="pp">println!</span>(<span class="st">&quot;one or two&quot;</span>),
    _ =&gt; <span class="pp">println!</span>(<span class="st">&quot;something else&quot;</span>),
<span class="op">}</span></code></pre></div>
<p>prints “one or two.” <code>if_chain!</code> supports this syntax in <code>if let</code> as well.</p></li>
</ul>
<p>Our example now <a href="https://github.com/lfairy/maud/blob/c849d9efdfa40565b4b0710036fa0da75b688f46/maud_macros/src/lints/doctype_html.rs#L23-L38">looks like this</a>:</p>
<div class="sourceCode"><pre class="sourceCode rust"><code class="sourceCode rust"><span class="pp">if_chain!</span> <span class="op">{</span>
    <span class="kw">if</span> <span class="kw">let</span> ExprCall(<span class="kw">ref</span> path_expr, <span class="kw">ref</span> args) = expr.node;
    <span class="kw">if</span> <span class="kw">let</span> <span class="cn">Some</span>(first_arg) = args.first();
    <span class="kw">if</span> <span class="kw">let</span> ExprLit(<span class="kw">ref</span> lit) = first_arg.node;
    <span class="kw">if</span> <span class="kw">let</span> LitKind::<span class="bu">Str</span>(s, _) = lit.node;
    <span class="kw">if</span> s.as_str().eq_ignore_ascii_case(<span class="st">&quot;&lt;!doctype html&gt;&quot;</span>);
    <span class="co">// ...</span>
    then <span class="op">{</span>
        <span class="co">// ...</span>
    <span class="op">}</span>
<span class="op">}</span></code></pre></div>
<p><a href="https://www.youtube.com/watch?v=fp5CkR5qSt0"><img src="/images/2016/delicious.jpg" alt="Yuuko Aioi raises a pair of chopsticks over her head" /></a></p>
<p>Delicious!</p><p><small>(Posted on <a href="/blog/if-chain/" title="link to this post"><time>Dec 29, 2016</time></a>.)</small></p></section></div><footer><p>Licensed under <a href="https://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>.</p></footer>
<script src="//static.getclicky.com/js"></script><script>try{ clicky.init(100874553); }catch(e){}</script>
<noscript><p><img alt="" width="1" height="1" src="//in.getclicky.com/100874553ns.gif"></p></noscript>
</body></html>