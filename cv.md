---
title: Curriculum Vitae
page-title: Chris Wong
---


<lambda.fairy@gmail.com> &emsp; [+64&nbsp;22&nbsp;620&nbsp;6369][mobile]  &emsp; [lambda.xyz][website] &emsp; [stackoverflow/617159][stackoverflow] &emsp; [github/lfairy][github]

[mobile]: tel:+64226206369
[website]: https://lambda.xyz
[stackoverflow]: https://stackoverflow.com/users/617159
[github]: https://github.com/lfairy


Education
=========

BSc(Hons) in Mathematics at the University of Canterbury; 2014--2016.

* Graduated with First Class Honours.
* Thesis: "Computable analysis via fast converging Cauchy sequences" ([pdf])

[pdf]: /images/2016/thesis.pdf


Experience
==========

## Telogis

Software Engineer, 2017--present

* Updating the Hours of Service (HoS) logging app to comply with United States regulations.


## Google

Software Engineering Intern, 2015--2016

* Developed tools for profiling the style engine in Chrome (Blink).


## University of Canterbury

Python Developer, 2014--2015

* Extended the online quiz system for the 200-level course "Formal Languages and Compilers".
* Implemented a pushdown automaton simulator, with documentation and tests.
* Used by 145 students in 2015.


## Google Summer of Code

Student, 2014

* Worked on [Hackage], the repository of software for the Haskell programming language.
* Added *build reports* to the interface, which indicated whether a package could be built on the server.

[Hackage]: https://hackage.haskell.org/


Projects
========

Most of this work can also be found on [GitHub].

[GitHub]: https://github.com/lfairy

[Maud](https://maud.lambda.xyz)
  : An HTML template engine implemented as a Rust compiler plugin. Templates are written inline using Rust syntax, and type-checked and optimized with the rest of the program.

[Arroyo](https://github.com/lfairy/arroyo)
  : Server that handles GitHub web hooks. Features authentication (via HMAC signatures) and simple configuration via shell scripts. Written in Python.

[Karkinos](https://karkinos.lambda.xyz)
  : Online directory of people interested in the Rust programming language. Features a bespoke full-text search engine and a mobile-first design.

[Guess the Number](https://lambda.xyz/gtn/)
  : A subversive number guessing game. Uses a bisecting algorithm to make the game impossible to win. Written in HTML and JavaScript/ES6.


Publications
============

* The Rust Programming Language. [RFC #940: Tweak the behavior of hyphens in package names][RFC 940].
* *Rust Design Patterns*. [Iterating over an Option][option-iter].
* *Rust Design Patterns*. [Prefer small crates][smol-crates].
* [Announcing `if_chain`][if-chain].
* [The fastest template engine in the West][maud-is-fast].
* [Shooting yourself in the foot][twelve-inches].

[RFC 940]: https://github.com/rust-lang/rfcs/blob/master/text/0940-hyphens-considered-harmful.md
[option-iter]: https://github.com/rust-unofficial/patterns/blob/master/idioms/option-iter.md
[smol-crates]: https://github.com/rust-unofficial/patterns/blob/master/patterns/small-crates.md
[if-chain]: https://lambda.xyz/blog/if-chain/
[maud-is-fast]: https://lambda.xyz/blog/maud-is-fast/
[twelve-inches]: https://lambda.xyz/blog/twelve-inches/


Achievements
============

* First Place in the New Zealand Programming Competition, 2012 and 2013.
* Reserve member of the New Zealand team, International Olympiad in Informatics, 2013.
* New Zealand Scholarship, 2013.


Activities
==========

* [*Rust Design Patterns*][patterns], Editor; 2016.
* University of Canterbury Music Club, Secretary; 2016.
* University of Canterbury Mathematics and Statistics Society, Secretary; 2015.

[patterns]: https://github.com/rust-unofficial/patterns
