---
title: Welcome
home: true
---

This is the personal website of Chris Wong, a student of mathematics and computer science living in Christchurch, New Zealand.

He likes [ducks], [turning code into butterflies], [writing things], and talking about himself in the third person. Not all at the same time, of course. That would be silly.

[ducks]: https://upload.wikimedia.org/wikipedia/commons/b/bf/Anas_platyrhynchos_male_female_quadrat.jpg
[turning code into butterflies]: https://github.com/lfairy
[writing things]: blog

As for the name: a *lambda fairy* is a creature which builds beautiful structures out of simple, composable parts. Chris is not a lambda fairy, but is quite enamored of the idea.

Feel free to look around. It won't bite, I promise.
